FROM python:3.9-slim-buster

LABEL Name="Python Flask Demo App"

WORKDIR /app
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY static /app/static
COPY templates /app/templates
COPY app.py /app
COPY Housing_Model /app

EXPOSE 5000

CMD ["gunicorn", "-b", "0.0.0.0:5000", "app:app"]