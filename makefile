test: venv  ## 🎯 Unit tests for Flask app
	. .venv/bin/activate \
	&& python -m unittest test_app.py

run: venv  ## 🏃 Run the server locally using Python & Flask
	. .venv/bin/activate \
	&& python app.py

venv: .venv/touchfile

.venv/touchfile: requirements.txt
	python3 -m venv .venv
	. .venv/bin/activate; pip install -Ur requirements.txt
	touch .venv/touchfile