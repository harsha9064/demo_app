import unittest
from app import app  # Make sure to adjust this import based on the actual path of your Flask app
import json

class FlaskTestCase(unittest.TestCase):
    # Ensure that Flask was set up correctly
    def test_index(self):
        tester = app.test_client(self)
        response = tester.get('/')
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)  # Check if the response is 200 OK
        #self.assertTrue(b'index.html' in response.data)  # Check if the response contains expected content

    # Test the predict function with POST
    def test_predict(self):
        tester = app.test_client(self)
        # Example data for posting to predict
        data = {
            'tradetime': '2020',
            'followers': '100',
            'square': '120',
            'livingroom': '1',
            'drawingroom': '1',
            'kitchen': '1',
            'bathroom': '1',
            'constructiontime': '2010',
            'communityaverage': '10000',
            'renovationcondition': 'renovationCondition_2',
            'buildingstructure': 'buildingStructure_2',
            'elevator': 'elevator_1'
        }
        response = tester.post('/', data=data)
        self.assertEqual(response.status_code, 200)
        # Additional assertions depending on the actual output of your app

if __name__ == '__main__':
    unittest.main()
